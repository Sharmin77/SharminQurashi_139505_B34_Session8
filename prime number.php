<?php
$start = 1;
$end = 500;
$count=1;
$fp = fopen("prime.txt", "w+");
for($num=$start; $num<=$end; $num++)
{
    if($num==1)continue;
    $isPrime=true;
    for($i=2; $i<=ceil($num/2); $i++)
    {
        if($num%$i==0)
        {
            $isPrime=false;
            break;
        }
    }

    if($isPrime)
    {
        echo "prime number" . $num . "<br>";
        fwrite($fp, "$num\t");
        if($count==10)
        {
            fwrite($fp,"\n");
            $count=0;
        }
        $count++;
        //fwrite($fp, "$num\t");
    }
}
fclose($fp);